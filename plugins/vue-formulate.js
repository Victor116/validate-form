import Vue from 'vue'
import VueFormulate from '@braid/vue-formulate'
import { es } from '@braid/vue-formulate-i18n'

export default {
  plugins: [es],
  locale: 'es'
}

Vue.use(VueFormulate,  {
  plugins: [es],
  locale: 'es'
})